package com.example.api_proyecto1;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;
import android.util.Log;

import com.example.api_proyecto1.controller.Facade;
import com.example.api_proyecto1.model.ClientUML;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Test;
import org.junit.runner.RunWith;

import static org.junit.Assert.*;

/**
 * Instrumented test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(AndroidJUnit4.class)
public class ExampleInstrumentedTest {

    private Facade f = Facade.getInstance();
    private final String TAG = "UNIT_TEST";
    private final String FILENAME = "test.puml";
    private final String UMLTEXT = "@startuml\nBob->Alice : hello\n@enduml";
    private final String DIRNAME = "testDir";
    private final String FILE_IN_DIR = DIRNAME + "/" + FILENAME;
    private final String IMGNAME = "testuml.png";

    /**
     * Return true if path exists, false otherwise.
     *
     * @param name
     * @return
     * @throws JSONException
     */
    private boolean pathExists(String dir, String name) throws JSONException {
        String files = f.getFiles(dir);
        JSONObject json = new JSONObject(files);
        JSONArray jarray = json.getJSONArray("children");
        boolean file_exists = false;
        for (int i = 0; i < jarray.length(); i++) {
            JSONObject o = jarray.getJSONObject(i);
            if (o.getString("name").equals(name)) {
                file_exists = true;
                break;
            }
        }
        return file_exists;
    }

    /**
     * Return true if path exists, false otherwise.
     *
     * @param name
     * @return
     * @throws JSONException
     */
    private boolean pathExists(String name) throws JSONException {
        return pathExists("", name);
    }


    /**
     * Default test.
     */
    @Test
    public void useAppContext() {
        // Context of the app under test.
        Context appContext = InstrumentationRegistry.getTargetContext();

        assertEquals("com.example.api_proyecto1", appContext.getPackageName());
    }


    /**
     * Create a file and test that exists.
     */
    @Test
    public void createFile() {
        f.createFile(FILENAME);
        try {
            assertTrue(pathExists(FILENAME));
        } catch (JSONException e) {
            e.printStackTrace();
            fail();
        }
    }


    /**
     * Edit a file and test that the content is the same.
     */
    @Test
    public void editFile() {
        createFile();
        f.editFile(FILENAME, UMLTEXT);
        Log.d(TAG, "Text to write:");
        Log.d(TAG, UMLTEXT);
        String textFromFile = f.getText(FILENAME);
        if (textFromFile.equals("")) {
            fail("The file is empty.");
        }
        Log.d(TAG, "Text from file:");
        Log.d(TAG, textFromFile);
        assertEquals(UMLTEXT, textFromFile);
    }


    /**
     * Delete a file and test if exists.
     */
    @Test
    public void deleteFile() {
        f.deleteFile(FILENAME);
        try {
            assertFalse(pathExists(FILENAME));
        } catch (JSONException e) {
            e.printStackTrace();
            fail();
        }
    }


    /**
     * Create a directory and test if exists.
     */
    @Test
    public void createDir() throws JSONException {
        f.createDir(DIRNAME);
        try {
            assertTrue(pathExists(DIRNAME));
        } catch (JSONException e) {
            e.printStackTrace();
            fail();
        }
    }


    /**
     * Create a directory and test if exists.
     */
    @Test
    public void deleteDir() throws JSONException {
        createDir();
        f.deleteDir(DIRNAME);
        try {
            assertFalse(pathExists(DIRNAME));
        } catch (JSONException e) {
            e.printStackTrace();
            fail();
        }
    }


    /**
     * Create a file in a directory and test if exists.
     */
    @Test
    public void createFileInDir() throws JSONException {
        f.createDir(DIRNAME);
        f.createFile(FILE_IN_DIR);
        try {
            assertTrue(pathExists(DIRNAME, FILENAME));
        } catch (JSONException e) {
            e.printStackTrace();
            fail();
        }
    }


    /**
     * Delete a file in a directory and test if exists.
     */
    @Test
    public void deleteFileInDir() throws JSONException {
        createFileInDir();
        f.deleteFile(FILE_IN_DIR);
        try {
            assertFalse(pathExists(DIRNAME, FILENAME));
        } catch (JSONException e) {
            e.printStackTrace();
            fail();
        }
    }


    /**
     * Tests if the path is correctly built.
     *
     * @throws JSONException
     */
    @Test
    public void testPathFormat() throws JSONException {
        createFileInDir();
        String files = f.getFiles(DIRNAME);
        try {
            JSONObject json = new JSONObject(files);
            JSONArray jarray = json.getJSONArray("children");
            JSONObject theFile = null;
            for (int i = 0; i < jarray.length(); i++) {
                theFile = jarray.getJSONObject(i);
                if (theFile.getString("name").equals(FILENAME)) {
                    break;
                }
            }
            System.out.println(theFile.getString("path"));
            assertEquals(theFile.getString("path"), FILE_IN_DIR);
        } catch (JSONException e) {
            e.printStackTrace();
            fail();
        }
    }


    /**
     * Test the plantuml client.
     */
    @Test
    public void getImageFromServer() {
        assertNotNull(ClientUML.getUML(UMLTEXT));
    }


    /**
     * Save an image and test if exists.
     */
    @Test
    public void saveImage() {
        f.createFile(IMGNAME);
        Bitmap img = ClientUML.getUML(UMLTEXT);
        f.saveImg(IMGNAME, img);
        System.out.println(f.getFiles());
        try {
            assertTrue(pathExists(IMGNAME));
        } catch (JSONException e) {
            e.printStackTrace();
            fail();
        }
    }

    /**
     * Save an image and test if exists.
     */
    @Test
    public void getImage() {
        f.createFile(IMGNAME);
        Bitmap img = ClientUML.getUML(UMLTEXT);
        f.saveImg(IMGNAME, img);
        System.out.println(f.getFiles());
        Bitmap img2 = f.getImage(IMGNAME);
        try {
            assertTrue(img.sameAs(img2));
        } catch (NullPointerException e) {
            e.printStackTrace();
            fail();
        }
    }

}
