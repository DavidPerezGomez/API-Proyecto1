package com.example.api_proyecto1;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.api_proyecto1.controller.Facade;

public class Main3Activity extends AppCompatActivity implements Dialognombre.dialogeditarlistener {

    EditText textoimportante;
    Button convert;
    Button text;
    Boolean editar = false;
    String nombreguardado = " ";
    String valor ="";
    Boolean regresa = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main3);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        textoimportante = findViewById(R.id.campo_6);
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            regresa = extras.getBoolean("regresa");
            editar = extras.getBoolean("editar");
            nombreguardado = extras.getString("nombrearchivo");
            valor = extras.getString("valor");
            if (regresa) {
                textoimportante.setText(valor);
            }
            else{
                if(editar){
                    textoimportante.setText(extras.getString("textoguardado"));
                }
                else{
                    textoimportante.setText(extras.getString("seleccion"));
                }
            }
        }
        convert = findViewById(R.id.convert);
        convert.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Main3Activity.this, Main4Activity.class);
                intent.putExtra("texto", textoimportante.getText().toString());
                startActivity(intent);
                finish();

            }
        });

        text = findViewById(R.id.buttonatext);
        text.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //guardar texto
                if (editar){
                    Facade facade = Facade.getInstance();
                    String filePath = facade.getSourcesPath() + nombreguardado;
                    facade.editFile(filePath, textoimportante.getText().toString());
                    Toast.makeText(Main3Activity.this,"texto guardado",Toast.LENGTH_LONG).show();
                }
                else {
                    Dialognombre dialog2 = new Dialognombre();
                    dialog2.show(((FragmentActivity) Main3Activity.this).getSupportFragmentManager(), "etiqueta");
                }

            }
        });

    }

    @Override
    public void editar(String lseleccion) {

                lseleccion = lseleccion + ".puml";
                Facade facade = Facade.getInstance();
                String filePath = facade.getSourcesPath() + lseleccion;
                facade.createFile(filePath);
                facade.editFile(filePath, textoimportante.getText().toString());
                finish();

    }
}
