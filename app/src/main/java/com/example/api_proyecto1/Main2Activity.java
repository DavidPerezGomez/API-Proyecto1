package com.example.api_proyecto1;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.example.api_proyecto1.controller.Facade;
import com.example.api_proyecto1.model.Diagrama;
import com.example.api_proyecto1.model.Archivotexto;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class Main2Activity extends AppCompatActivity implements Dialogelegir.dialogelegirlistener {

    TextView text1;
    TextView text2;
    RecyclerView rv1;
    RecyclerView rv2;
    private String nombreabuscar;
    private boolean eliminar = false;
    private ArrayList<Diagrama> diagramas = new ArrayList<>();
    private ArrayList<Archivotexto> textos = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        text1 = findViewById(R.id.textView1);
        text2 = findViewById(R.id.textView2);
        rv1 = findViewById(R.id.rv1);
        rv2 = findViewById(R.id.rv2);

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            // recojemos cosas de los intents
        }

        BottomNavigationView bottomNavigationView = (BottomNavigationView)
                                                            findViewById(R.id.navigation);
        bottomNavigationView.setOnNavigationItemSelectedListener(
                new BottomNavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.action_buscar:
                                break;
                            case R.id.action_add:
                                AlertDialog.Builder builder = new AlertDialog.Builder(Main2Activity.this);
                                builder.setTitle("Elige plantilla para el diagrama");
                                CharSequence[] opciones = {"En blanco", "Secuencia", "Clases"};
                                builder.setItems(opciones, new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        String text = "";
                                        if (which == 0) {  // en blanco
                                            text = "";
                                        } else if (which == 1) {
                                            text = "Bob -> Alice: something()";
                                        } else if (which == 2) {
                                            text = "class Dummy {\n" +
                                                    "  String data\n" +
                                                    "  void methods()\n" +
                                                    "}";
                                        }
                                        Intent intent = new Intent(Main2Activity.this, Main3Activity.class);
                                        intent.putExtra("seleccion", text);
                                        startActivity(intent);
                                    }
                                });
                                builder.show();
                                break;
                            case R.id.action_eliminar:

                                if (eliminar) {
                                    eliminar = false;
                                    initadapater(eliminar, diagramas, textos);
                                } else {
                                    eliminar = true;
                                    initadapater(eliminar, diagramas, textos);
                                }
                                break;
                        }
                        return true;
                    }
                });

    }

    @Override
    protected void onResume() {
        super.onResume();

        inicializarlistas();
        initadapater(eliminar, diagramas, textos);
    }

    public void inicializarlistas() {
        Facade facade = Facade.getInstance();
        try {
            JSONObject jsonFilesDir = new JSONObject(facade.getFiles(facade.getSourcesPath()));
            System.out.println(jsonFilesDir.toString(2));
            JSONArray jsonFiles = jsonFilesDir.getJSONArray("children");
            textos = new ArrayList<>();
            for (int i = 0; i < jsonFiles.length(); i++) {
                JSONObject jsonFile = jsonFiles.getJSONObject(i);
                if (jsonFile.getString("type").equals("plantuml")) {
                    String name = jsonFile.getString("name");
                    String text = facade.getText(jsonFile.getString("path"));
                    Archivotexto file = new Archivotexto(text, name);
                    textos.add(file);
                }
            }

            JSONObject jsonImagesDir = new JSONObject(facade.getFiles(facade.getImagesPath()));
            System.out.println(jsonImagesDir.toString(2));
            JSONArray jsonImages = jsonImagesDir.getJSONArray("children");
            diagramas = new ArrayList<>();
            for (int i = 0; i < jsonImages.length(); i++) {
                JSONObject jsonFile = jsonImages.getJSONObject(i);
                if (jsonFile.getString("type").equals("img")) {
                    String name = jsonFile.getString("name");
                    Bitmap img = facade.getImage(jsonFile.getString("path"));
                    Diagrama diagram = new Diagrama(img, name);
                    diagramas.add(diagram);
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void initadapater(boolean eliminar, final ArrayList<Diagrama> pdiagramas, final ArrayList<Archivotexto> lostxs) {

        final boolean elim = eliminar;
        final Eladap eladaptador1 = new Eladap(lostxs, eliminar);
        eladaptador1.setOnClickListener(new View.OnClickListener() {
            @Override

            public void onClick(View v) {
                final int index = rv1.getChildLayoutPosition(v);
                if (elim) {
                    Facade facade = Facade.getInstance();
                    facade.deleteFile(facade.getSourcesPath() + lostxs.get(index).getNombre());
                    lostxs.remove(index);
                    eladaptador1.notifyItemRemoved(index);
                } else {
                    nombreabuscar = lostxs.get(index).getNombre();
                    Facade facade = Facade.getInstance();
                    String texto = facade.getText(facade.getSourcesPath() + nombreabuscar);
                    Intent e = new Intent(Main2Activity.this,Main3Activity.class);
                    e.putExtra("textoguardado",texto);
                    e.putExtra("nombrearchivo",nombreabuscar);
                    e.putExtra("editar",true);
                    startActivity(e);
                }
            }
        });
        rv1.setAdapter(eladaptador1);
        LinearLayoutManager elLayoutLineal = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        rv1.setLayoutManager(elLayoutLineal);

        final ElAdaptadorRecycler eladaptador = new ElAdaptadorRecycler(pdiagramas, eliminar);
        eladaptador.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final int index1 = rv2.getChildLayoutPosition(v);
                if (elim) {
                    Facade facade = Facade.getInstance();
                    facade.deleteFile(facade.getImagesPath() + pdiagramas.get(index1).getNombre());
                    pdiagramas.remove(index1);
                    eladaptador.notifyItemRemoved(index1);
                } else {
                    nombreabuscar = pdiagramas.get(index1).getNombre();
                    Intent b = new Intent(Main2Activity.this,Main4Activity.class);
                    b.putExtra("nombrearchivo",nombreabuscar);
                    b.putExtra("ver",true);
                    startActivity(b);
                }
            }
        });
        rv2.setAdapter(eladaptador);
        LinearLayoutManager elLayoutLinea2 = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        rv2.setLayoutManager(elLayoutLinea2);
    }



    @Override
    public void applyTexts(String seleccion) {
       // Intent intent = new Intent(Main2Activity.this, Main3Activity.class);
       // intent.putExtra("seleccion",seleccion);
       // startActivity(intent);
       // finish();
    }
}
