package com.example.api_proyecto1;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.StrictMode;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.LinearInterpolator;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.example.api_proyecto1.controller.Facade;
import com.example.api_proyecto1.model.Diagrama;

public class Main4Activity extends AppCompatActivity implements Dialognombre.dialogeditarlistener {

    private ImageView imagen;
    private String valor;
    private Diagrama diagram;
    private Button bt;
    float scalediff;
    private static final int NONE = 0;
    private static final int DRAG = 1;
    private static final int ZOOM = 2;
    private int mode = NONE;
    private float oldDist = 1f;
    private float d = 0f;
    private float newRot = 0f;
    private boolean ver = false;
    String nombrebuscado = "";
    Bitmap img1 ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        setContentView(R.layout.activity_main4);
        imagen = findViewById(R.id.imagen1);
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            valor = extras.getString("texto");
            ver = extras.getBoolean("ver");
            nombrebuscado = extras.getString("nombrearchivo");
        }
        if (ver) {
            Facade facade = Facade.getInstance();
            Bitmap img2 = facade.getImage(facade.getImagesPath() + nombrebuscado);
            imagen.setImageBitmap(img2);
        }else{
            img1 = Facade.getInstance().getUML(valor);
            imagen.setImageBitmap(img1);
        }


        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(500, 500);
        layoutParams.leftMargin = 200;
        layoutParams.topMargin = 200;
        layoutParams.bottomMargin = -250;
        layoutParams.rightMargin = -250;
        imagen.setLayoutParams(layoutParams);

        imagen.setOnTouchListener(new View.OnTouchListener() {
                                      RelativeLayout.LayoutParams parms;
                                      int startwidth;
                                      int startheight;
                                      float dx = 0, dy = 0, x = 0, y = 0;
                                      float angle = 0;

                                      @Override
                                      public boolean onTouch(View v, MotionEvent event) {
                                          final ImageView view = (ImageView) v;

                                          ((BitmapDrawable) view.getDrawable()).setAntiAlias(true);
                                          switch (event.getAction() & MotionEvent.ACTION_MASK) {
                                              case MotionEvent.ACTION_DOWN:

                                                  parms = (RelativeLayout.LayoutParams) view.getLayoutParams();
                                                  startwidth = parms.width;
                                                  startheight = parms.height;
                                                  dx = event.getRawX() - parms.leftMargin;
                                                  dy = event.getRawY() - parms.topMargin;
                                                  mode = DRAG;
                                                  break;

                                              case MotionEvent.ACTION_POINTER_DOWN:
                                                  oldDist = spacing(event);
                                                  if (oldDist > 10f) {
                                                      mode = ZOOM;
                                                  }

                                                  d = rotation(event);

                                                  break;
                                              case MotionEvent.ACTION_UP:

                                                  break;

                                              case MotionEvent.ACTION_POINTER_UP:
                                                  mode = NONE;

                                                  break;
                                              case MotionEvent.ACTION_MOVE:
                                                  if (mode == DRAG) {

                                                      x = event.getRawX();
                                                      y = event.getRawY();

                                                      parms.leftMargin = (int) (x - dx);
                                                      parms.topMargin = (int) (y - dy);

                                                      parms.rightMargin = 0;
                                                      parms.bottomMargin = 0;
                                                      parms.rightMargin = parms.leftMargin + (5 * parms.width);
                                                      parms.bottomMargin = parms.topMargin + (10 * parms.height);

                                                      view.setLayoutParams(parms);

                                                  } else if (mode == ZOOM) {

                                                      if (event.getPointerCount() == 2) {

                                                          newRot = rotation(event);
                                                          float r = newRot - d;
                                                          angle = r;

                                                          x = event.getRawX();
                                                          y = event.getRawY();

                                                          float newDist = spacing(event);
                                                          if (newDist > 10f) {
                                                              float scale = newDist / oldDist * view.getScaleX();
                                                              if (scale > 0.6) {
                                                                  scalediff = scale;
                                                                  view.setScaleX(scale);
                                                                  view.setScaleY(scale);

                                                              }
                                                          }

                                                          view.animate().rotationBy(angle).setDuration(0).setInterpolator(new LinearInterpolator()).start();

                                                          x = event.getRawX();
                                                          y = event.getRawY();

                                                          parms.leftMargin = (int) ((x - dx) + scalediff);
                                                          parms.topMargin = (int) ((y - dy) + scalediff);

                                                          parms.rightMargin = 0;
                                                          parms.bottomMargin = 0;
                                                          parms.rightMargin = parms.leftMargin + (5 * parms.width);
                                                          parms.bottomMargin = parms.topMargin + (10 * parms.height);

                                                          view.setLayoutParams(parms);


                                                      }
                                                  }
                                                  break;
                                          }

                                          return true;
                                      }
                                  });
        bt = findViewById(R.id.butonguardar);
        bt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(ver){
                    Toast.makeText(Main4Activity.this,"imagen guardada",Toast.LENGTH_LONG).show();
                } else {
                    Dialognombre dialog1 = new Dialognombre();
                    dialog1.show(((FragmentActivity) Main4Activity.this).getSupportFragmentManager(), "etiqueta");

                }
            }
        });

    }


    private float spacing(MotionEvent event) {
        float x = event.getX(0) - event.getX(1);
        float y = event.getY(0) - event.getY(1);
        return (float) Math.sqrt(x * x + y * y);
    }

    private float rotation(MotionEvent event) {
        double delta_x = (event.getX(0) - event.getX(1));
        double delta_y = (event.getY(0) - event.getY(1));
        double radians = Math.atan2(delta_y, delta_x);
        return (float) Math.toDegrees(radians);
    }


    @Override
    public void editar(String lseleccion) {

        lseleccion = lseleccion + ".png";
        diagram = new Diagrama(img1, lseleccion);
        Intent i = new Intent(Main4Activity.this, Main3Activity.class);
        i.putExtra("valor",valor);
        i.putExtra("regresa",true);
        Facade facade = Facade.getInstance();
        String imgPath = facade.getImagesPath() + diagram.getNombre();
        facade.createFile(imgPath);
        facade.saveImg(imgPath, diagram.getImagen());
        Toast.makeText(Main4Activity.this,"imagen guardada",Toast.LENGTH_LONG).show();
        startActivity(i);
        finish();

    }
}
