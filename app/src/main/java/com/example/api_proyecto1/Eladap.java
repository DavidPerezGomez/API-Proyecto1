package com.example.api_proyecto1;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.api_proyecto1.model.Archivotexto;

import java.util.ArrayList;

public class Eladap extends RecyclerView.Adapter <Eladap.ElViewHolder> implements View.OnClickListener {

    public ArrayList<Archivotexto> lostextos;
    private Boolean eliminar;
    private View.OnClickListener listener;

    public Eladap(ArrayList<Archivotexto> textos, Boolean peliminar) {
        this.lostextos = textos;
        this.eliminar = peliminar;
    }

    public ElViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        if(eliminar){
            View ellayoutdelafila = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.recyclerdiagram2, null);
            ellayoutdelafila.setOnClickListener(this);
            ElViewHolder evh1 = new ElViewHolder(ellayoutdelafila);
            return evh1;
        }
        View ellayoutdelafila = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.card_viex, null);
        ellayoutdelafila.setOnClickListener(this);
        ElViewHolder evh = new ElViewHolder(ellayoutdelafila);
        return evh;
    }

    @Override
    public void onBindViewHolder(@NonNull ElViewHolder elViewHolder, int i) {
        if(eliminar){
            elViewHolder.conboton = true;
        }
        elViewHolder.eltexto.setText(lostextos.get(i).getNombre());
        elViewHolder.img.setImageResource(R.drawable.filedoc);

    }

    @Override
    public int getItemCount() {
        return lostextos.size();
    }

    public void onClick(View v) {
        if (listener != null) {
            listener.onClick(v);
        }
    }

    public void setOnClickListener(View.OnClickListener listener) {
        this.listener = listener;
    }



    public class ElViewHolder extends RecyclerView.ViewHolder {
    public TextView eltexto;
    public ImageView img;
    public ImageView img2;
    public boolean conboton = false;

    public ElViewHolder(View v) {
        super(v);
        img = v.findViewById(R.id.proyectImage);
        eltexto = v.findViewById(R.id.proyectName);
        if (conboton) {
            img2 = v.findViewById(R.id.proyectImage2);

        }

    }
}}