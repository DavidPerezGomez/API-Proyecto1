package com.example.api_proyecto1;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatDialogFragment;
import android.view.LayoutInflater;
import android.view.View;

import java.util.ArrayList;

public class Dialogelegir extends AppCompatDialogFragment {
    View view;
    final CharSequence[] opciones = {"En blanco","Diagrama de clases", "Diagrama de secuencia"};
    public dialogelegirlistener listener;
    public boolean haelegido = false;

    public void setSeleccion(String seleccion) {
        this.seleccion = seleccion;
    }

    private String seleccion = "X";

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        //Si restauran los valores de la seleccion y del boolean haelegido
        if (savedInstanceState != null) {
            seleccion = savedInstanceState.getString("lseleccion");
            haelegido = savedInstanceState.getBoolean("haelegido");
        }

        super.onCreateDialog(savedInstanceState);
        listener = (dialogelegirlistener) getActivity();
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();

        builder.setItems(opciones, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // si esta marcado se añade a la lista de elegidos, si ya estaba en la lista se remueve
                if (which == 0) {  // en blanco
                     setSeleccion("");
                } else if (which == 1) {
                    setSeleccion("Bob -> Alice: something()");
                } else if (which == 2) {
                    setSeleccion("class Dummy {\nString data\n+void methods()\n}");
                }
                haelegido=true;
            }
        });
        builder.setView(view)
                .setTitle("Elegir plantilla")
                .setPositiveButton("ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (haelegido == true) {
                            listener.applyTexts(seleccion);
                        } else {
                            //si no ha elegido, no hace nada
                        }

                    }
                });


        return builder.create();
    }


    public interface dialogelegirlistener {
        //listener al que se le pasa el producto elegido
        void applyTexts(String seleccion);

    }
    public void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);

        savedInstanceState.putString("lseleccion",seleccion);
        savedInstanceState.putBoolean("haelegido",true);


    }

}