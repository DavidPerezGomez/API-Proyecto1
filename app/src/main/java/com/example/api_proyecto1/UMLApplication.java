package com.example.api_proyecto1;

import android.app.Application;
import android.content.Context;

public class UMLApplication extends Application {

    private static Context context;

    public void onCreate() {
        super.onCreate();
        UMLApplication.context = getApplicationContext();
    }

    public static Context getAppContext() {
        return UMLApplication.context;
    }
}