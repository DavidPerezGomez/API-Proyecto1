package com.example.api_proyecto1;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class Dialognombre extends DialogFragment {

    private View view;
    private EditText editar;
    public Dialognombre.dialogeditarlistener listener;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        listener = (Dialognombre.dialogeditarlistener) getActivity();
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();

        view = inflater.inflate(R.layout.dialogo2, null);
        editar = (EditText) view.findViewById(R.id.edit);
        builder.setView(view)
                .setPositiveButton("", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        String valor = editar.getText().toString();
                        if((valor == null) || (valor.equals(""))){
                           // listener.editar(valor);
                            Toast.makeText(getActivity(),"Añadir nombre al archivo",Toast.LENGTH_LONG).show();
                        }else{
                            listener.editar(valor);
                            //Toast.makeText(getActivity(),"Añadir nombre al archivo",Toast.LENGTH_LONG).show();
                        }

                    }
                })
                .setPositiveButtonIcon(getActivity().getResources().getDrawable(
                android.R.drawable.ic_menu_save))
                .setNegativeButtonIcon(getActivity().getResources().getDrawable(android.R.drawable.ic_menu_revert));



        return builder.create();
    }


    interface dialogeditarlistener {

        void editar(String lseleccion);

    }

}