package com.example.api_proyecto1.model;

import android.graphics.Bitmap;

public class Archivotexto {
    private String texto;
    private String  nombre;

    public Archivotexto(String ptexto, String pnombre){
        this.texto = ptexto;
        this.nombre = pnombre;
    }
    public String getNombre() {
        return nombre;
    }
}
