package com.example.api_proyecto1.model;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import com.example.api_proyecto1.UMLApplication;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.*;

public class FileManager {

    private static FileManager instance;

    private static final String SOURCES_PATH = "sources/";
    private static final String IMAGES_PATH = "img/";

    private File root;

    private FileManager() {
        this.root = UMLApplication.getAppContext().getFilesDir();
        this.init();
    }

    public static FileManager getInstance() {
        if (instance == null) {
            instance = new FileManager();
        }
        return instance;
    }

    public String getImagesPath() {
        return IMAGES_PATH;
    }

    public String getSourcesPath() {
        return SOURCES_PATH;
    }

    private void init() {
        // access the app directories to ensure they exist
        // if a directory doesn't exist it is created
        String pathImg = IMAGES_PATH;
        File dirImg = getFile(pathImg);
        if (!(dirImg != null && dirImg.isDirectory())) {
            this.createDir(pathImg);
        }
        String pathSrc = SOURCES_PATH;
        File dirSrc = getFile(pathSrc);
        if (!(dirSrc != null && dirSrc.isDirectory())) {
            this.createDir(pathSrc);
        }
    }

    private File getFile(String filePath) {
        File file = new File(root, filePath);
        if (file.exists()) {
            return file;
        } else {
            return null;
        }
    }

    private JSONObject fileToJSON(File file) {
        JSONObject fileJSON = new JSONObject();
        try {
            fileJSON.put("name", file.getName());
            String path = file.getAbsolutePath();
            String tmp = "/files/";
            int indexRoot = path.indexOf(tmp);
            path = path.substring(indexRoot + tmp.length());
            fileJSON.put("path", path);
            String type = "";
            if (file.isDirectory()) {
                type = "dir";
            } else {
                int dotIndex = file.getName().indexOf('.');
                if (dotIndex != -1) {
                    String extension = file.getName().substring(dotIndex);
                    if (extension.equals(".puml")) {
                        type = "plantuml";
                    } else if (extension.equals(".png")) {
                        type = "img";
                    }
                }
            }
            fileJSON.put("type", type);

            JSONArray childrenJSON = new JSONArray();
            File[] children = file.listFiles();

            if (children != null) {
                for (File child : children) {
                    JSONObject childJSON = fileToJSON(child);
                    childrenJSON.put(childJSON);
                }
            }

            fileJSON.put("children", childrenJSON);
        } catch (JSONException e) {
            e.printStackTrace();
//        } catch (IOException e) {
//            e.printStackTrace();
        }
        return fileJSON;
    }

    private boolean deleteDir(File dir) {
        if (dir != null && dir.isDirectory()) {
            File[] children = dir.listFiles();

            if (children != null) {
                for (File child : children) {
                    if (child.isFile()) {
                        child.delete();
                    } else if (child.isDirectory()) {
                        deleteDir(child);
                    }
                }
            }

            return dir.delete();
        }

        return false;
    }

    /**
     * Returns a JSON-formatted string describing the root directory file and it's children.
     * <p>
     * If the file doesn't exist, the result string will be empty.
     *
     * @return The JSON-formatted string describing the root directory
     */
    public String getFiles() {
        return getFiles("");
    }

    /**
     * Returns a JSON-formatted string describing the file and it's children.
     * If the given path is empty, the root directory will be used by default.
     * <p>
     * If the file doesn't exist, the result string will be empty.
     *
     * @param dirPath Path of the file to describe
     * @return The JSON-formatted string describing the file
     */
    public String getFiles(String dirPath) {
        File dir = root;
        if (!dirPath.equals("")) {
            dir = getFile(dirPath);
            if (dir == null) {
                return "";
            }
        }
        return fileToJSON(dir).toString();
    }

    /**
     * Deletes the specified file.
     *
     * @param filePath Path to the file to delete
     * @return True if the file was deleted successfully, False otherwise.
     */
    public boolean deleteFile(String filePath) {
        File file = this.getFile(filePath);
        if (file != null && file.isFile()) {
            return file.delete();
        }
        return false;
    }

    /**
     * Deletes the specified directory.
     *
     * @param dirPath Path to the directory to delete
     * @return True if the directory was deleted successfully, False otherwise.
     */
    public boolean deleteDir(String dirPath) {
        File dir = this.getFile(dirPath);
        if (dir != null && dir.isDirectory()) {
            return deleteDir(dir);
        }
        return false;
    }

    /**
     * Returns a string containing the contents of the file.
     * <p>
     * If the file doesn't exist or is not a directory the resulting
     * string will be empty.
     *
     * @param filePath Path of the file to read
     * @return A string with the contents of the file.
     */
    public String getText(String filePath) {
        File file = getFile(filePath);
        StringBuilder text = new StringBuilder();
        if (file != null && file.isFile()) {
            try {
                BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(file)));
                String line;
                while ((line = br.readLine()) != null) {
                    text.append(line);
                    text.append("\n");
                }
                if(text.length() > 0) {
                    text.deleteCharAt(text.length() - 1);
                }
                br.close();
                return text.toString();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return "";
    }

    /**
     * Returns a Bitmap generated from the contents of the file.
     * <p>
     * If the file doesn't exist or is not a directory the
     * result will be null
     *
     * @param filePath Path of the file to read
     * @return A Bitmap from the contents of the file.
     */
    public Bitmap getImage(String filePath) {
        File file = getFile(filePath);
        Bitmap img = null;
        if (file != null && file.isFile()) {
            img = BitmapFactory.decodeFile(file.getPath());
        }
        return img;
    }

    /**
     * Sets the given text as the content for the specified file.
     * This overrides all the previous contents of the file.
     *
     * @param filePath Path to the file to be written in
     * @param text     Text to be written in the file
     * @return True if the text is successfully written, False otherwise
     */
    public boolean editFile(String filePath, String text) {
        File file = getFile(filePath);
        if (file != null && file.isFile()) {
            try {
                OutputStreamWriter ow = new OutputStreamWriter(new FileOutputStream(file));
                ow.write(text);
                ow.close();
                return true;
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return false;
    }

    /**
     * Stores the given Bitmap as a png image in the specified file.
     *
     * @param filePath Path to the file to save the image in
     * @param img      Bitmap of the image to save
     * @return True if the image is successfully saved, False otherwise
     */
    public boolean saveImg(String filePath, Bitmap img) {
        File file = getFile(filePath);
        if (img != null && file != null && file.isFile()) {
            try {
                FileOutputStream out = new FileOutputStream(file);
                img.compress(Bitmap.CompressFormat.PNG, 100, out);
                out.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return false;
    }

    /**
     * Creates the specified empty file.
     *
     * @param filePath Path to the file to create
     * @return True if the file was created successfully, False otherwise.
     */
    public boolean createFile(String filePath) {
        try {
            File dir = new File(root, filePath);
            return dir.createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return false;
    }

    /**
     * Creates the specified directory under root.
     *
     * @param dirPath Path of the directory to create
     * @return True if the directory was created successfully, False otherwise
     */
    public boolean createDir(String dirPath) {
        File dir = new File(root, dirPath);
        return dir.mkdirs();
    }

}
