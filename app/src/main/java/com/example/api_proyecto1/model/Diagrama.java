package com.example.api_proyecto1.model;

import android.graphics.Bitmap;

public class Diagrama {

    private Bitmap imagen;
    private String  nombre;

    public Diagrama(Bitmap pimagen, String pnombre){
        this.imagen = pimagen;
        this.nombre = pnombre;
    }

    public Bitmap getImagen() {
        return imagen;
    }
    public String getNombre() {
        return nombre;
    }
    public void setImagen(Bitmap imagen) {
        this.imagen = imagen;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

}
