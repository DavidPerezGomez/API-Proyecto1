package com.example.api_proyecto1.model;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import net.sourceforge.plantuml.code.ArobaseStringCompressor;
import net.sourceforge.plantuml.code.AsciiEncoder;
import net.sourceforge.plantuml.code.CompressionHuffman;
import net.sourceforge.plantuml.code.TranscoderImpl;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public abstract class ClientUML {

    public static Bitmap getUML(String text) {
        // in case of StrictMode$AndroidBlockGuardPolicy exception break cristal
        // https://stackoverflow.com/questions/22395417/error-strictmodeandroidblockguardpolicy-onnetwork

        String encodedUMLText = null;
        TranscoderImpl t = new TranscoderImpl(new AsciiEncoder(), new ArobaseStringCompressor(), new CompressionHuffman());
        try {
            encodedUMLText = t.encode(text);
        } catch (IOException e) {
            e.printStackTrace();
        }

        String imageUrl = "http://www.plantuml.com/plantuml/png/" + encodedUMLText;

        URL url;
        try {
            url = new URL(imageUrl);
        } catch (MalformedURLException e) {
            e.printStackTrace();
            return null;
        }

        HttpURLConnection connection;
        try {
            connection = (HttpURLConnection) url.openConnection();
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }

        InputStream is;
        try {
            is = connection.getInputStream();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return null;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }

        Bitmap img = BitmapFactory.decodeStream(is);

        return img;
    }
}
