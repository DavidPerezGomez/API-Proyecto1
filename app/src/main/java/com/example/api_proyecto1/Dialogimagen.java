package com.example.api_proyecto1;

import android.app.Dialog;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatDialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

public class Dialogimagen extends AppCompatDialogFragment {

    private View view;
    private TextView editar;
    private ImageView img;
    public Dialogimagen.dialogimagenlistener listener;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        listener = (Dialogimagen.dialogimagenlistener) getActivity();
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();

        view = inflater.inflate(R.layout.recyclerdiagram, null);
        img = view.findViewById(R.id.imagen1);
        img.setImageBitmap(listener.applyimagen());
        builder.setView(view)
                .setTitle("Diagrama")
                .setPositiveButton("ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dismiss();
                    }
                });


        return builder.create();
    }


    interface dialogimagenlistener {

        Bitmap applyimagen();

    }


}